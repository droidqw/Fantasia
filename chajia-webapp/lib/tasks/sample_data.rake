namespace :db do
	desc "Fill database with sample data"
	task populate: :environment do

		make_prices
	end

	def make_prices
		
		1.times do |n|
			product_id = 2248
			price = get_randomdate
			created_at =Date.new(2014,1,5)+n
			updated_at=created_at
			Price.create!(product_id: product_id,price: price,created_at: created_at,updated_at: updated_at)
			
		end
	end

	def get_randomdate
		Random.new.rand(200.00..500.00).to_s.match(/\d+.\d\d/).to_s
	end

end