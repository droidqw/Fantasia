class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :pid
      t.string :title
      t.string :url
      t.string :img
      t.string :state
      t.timestamps
    end
  end
end
