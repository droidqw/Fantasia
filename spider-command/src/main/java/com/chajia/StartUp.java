package com.chajia;

import com.chajia.dao.ProductDao;
import com.chajia.pageprocess.EtaoProcess;
import com.chajia.pipeline.MysqlPipeline;
import com.chajia.util.LogUtil;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;


import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午4:12
 * To change this template use File | Settings | File Templates.
 */
class StartUp {
    public static Class clazz = StartUp.class;

    public static void main(String[] args) {
        try {
            StartLoader.database();
            Spider.create(new EtaoProcess()).thread(10).addUrl("http://s.etao.com/detail/35031005935.html")
                    .addPipeline(new MysqlPipeline()).run();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
